class Git {
    constructor() {
        this._featureColors = ['#8BC34A', '#FFC107', '#FF5722', '#E91E63', '#AB47BC'];
        this._branches = new Map();
        this._features = new Map();
        this._promotions = [];
        // Assign instance to global
        window.git = this;
        const options = {
            template: GitgraphJS.templateExtend('metro', {
                colors: ['#1976D2', '#03A9F4', '#0097A7', '#26C6DA'],
                commit: {
                    message: {
                        displayAuthor: false,
                    },
                },
                branch: {
                    label: {
                        bgColor: '#25282c',
                    },
                },
            }),
        };
        // Instantiate the graph.
        const gitgraph = GitgraphJS.createGitgraph(document.getElementById('graph-container'), options);
        this._branches.set('main', gitgraph.branch('main').commit('PROD Snapshot'));
        this._branches.set('qa', this._branches.get('main').branch('qa').commit('QA Snapshot'));
        this._branches.set('dev1', this._branches.get('qa').branch('dev1').commit('DEV1 Snapshot'));
        this._branches.set('dev2', this._branches.get('qa').branch('dev2').commit('DEV2 Snapshot'));
    }
    createUserStory(org) {
        const featureNumber = this._features.size + 1;
        const feature = {
            name: `feature/US-${featureNumber}`,
            number: featureNumber,
            color: this._featureColors[(featureNumber - 1) % this._featureColors.length],
        };
        this._features.set(feature.name, feature);
        const branch = this._branchColored('main', feature.name, feature.color).commit(`${feature.name}: Meaningful commit message`);
        this._branches.set(feature.name, branch);
        this._branches.get(org).merge(feature.name);
    }
    deployToOrg(org) {
        const orgTitle = document.getElementById('deployToOrg-org');
        orgTitle.textContent = org;
        const checkboxes = document.getElementById('deployToOrg-stories');
        // clear all child nodes first
        checkboxes.querySelectorAll('*').forEach((n) => n.remove());
        this._features.forEach((feature) => {
            const checkboxRow = document.createElement('div');
            const inputCol = document.createElement('div');
            inputCol.classList.add(...['col-sm', 'px-5', 'mb-5']);
            inputCol.style.border = `2px solid ${feature.color}`;
            inputCol.style.borderRadius = '10px';
            inputCol.style.color = `${feature.color}`;
            const input = document.createElement('input');
            input.type = 'checkbox';
            input.id = feature.name;
            input.classList.add(...['mr-5']);
            const label = document.createElement('label');
            label.htmlFor = input.id;
            label.textContent = feature.name;
            inputCol.appendChild(input);
            inputCol.appendChild(label);
            checkboxRow.appendChild(inputCol);
            checkboxes.appendChild(checkboxRow);
        });
        halfmoon.toggleModal('deployToOrg');
    }
    promoteAndDeploy() {
        const org = document.getElementById('deployToOrg-org').textContent;
        const checkboxes = document.getElementById('deployToOrg-stories');
        const storiesToPromote = [];
        checkboxes.querySelectorAll('div > input').forEach((input) => {
            if (input.checked) {
                storiesToPromote.push(input.id);
            }
        });
        if (storiesToPromote.length > 0) {
            // Create Promotion Branch
            const promotion = `promotion/P-${this._promotions.length + 1}`;
            this._promotions.push(promotion);
            this._branchColored(org, promotion, '#BDBDBD');
            storiesToPromote.forEach((story) => {
                const options = {
                    branch: story,
                    commitOptions: {
                        style: {
                            dot: {
                                color: this._features.get(story).color,
                            },
                        },
                    },
                };
                this._branches.get(promotion).merge(options);
            });
            this._branches.get(org).merge(promotion);
        }
    }
    _branchColored(base, name, color) {
        const branch = this._branches.get(base).branch({
            name: name,
            style: {
                color: color,
                label: {
                    strokeColor: color,
                },
            },
            commitDefaultOptions: {
                style: {
                    color: color,
                    message: {
                        color: color,
                    },
                    dot: {
                        color: color,
                    },
                },
            },
        });
        this._branches.set(branch.name, branch);
        return branch;
    }
}
Git.instance = new Git();
export { Git };
